package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	IGrid currentGeneration;

	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

    @Override
    public CellState getCellState(int row, int column) {
		return currentGeneration.get(row, column);
    }

    @Override
    public IGrid getGrid() {
		return currentGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
		CellState state = currentGeneration.get(row, col);
		int numAlive = countNeighbors(row, col, CellState.ALIVE);
		return switch (state) {
			case ALIVE -> CellState.DYING;
			case DEAD -> numAlive == 2 ? CellState.ALIVE : CellState.DEAD;
			case DYING -> CellState.DEAD;
		};
    }

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for (int i : new int[]{-1, 0, 1}) {
			for (int j : new int[]{-1, 0, 1}) {
				if (i == 0 && j == 0) {
					continue;
				}
				CellState currentState = currentGeneration.get(
						Math.floorMod(row +i, numberOfRows())
						, Math.floorMod(col + j, numberOfColumns()));
				if (currentState.equals(state)) {
					count++;
				}
			}
		}
		return count;
	}
	// private int countNeighbors(int row, int col, CellState state) {
	// 	int count = 0;
	// 	for (int i : new int[]{-1, 0, 1}) {
	// 		if (row + i < 0
	// 				|| row + i >= numberOfRows()) {
	// 			continue;
	// 		}
	// 		for (int j : new int[]{-1, 0, 1}) {
	// 			if (col + j < 0
	// 					|| col + j >= numberOfColumns()) {
	// 				continue;
	// 			}
	// 			if (i == 0 && j == 0) {
	// 				continue;
	// 			}
	// 			if (currentGeneration.get(row + i, col + j).equals(state)) {
	// 				count++;
	// 			}
	// 		}
	// 	}
	// 	return count;
	// }

    @Override
    public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				CellState state = switch (random.nextInt(3)) {
					case 0 -> CellState.ALIVE;
					case 1 -> CellState.DEAD;
					case 2 -> CellState.DYING;
					default -> CellState.ALIVE;
				};
				currentGeneration.set(row, col, state);
			}
		}
    }

    @Override
    public int numberOfColumns() {
		return currentGeneration.numColumns();
    }

    @Override
    public int numberOfRows() {
		return currentGeneration.numRows();
    }

    @Override
    public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < nextGeneration.numRows(); row++) {
			for (int column = 0; column < nextGeneration.numColumns(); column++) {
				nextGeneration.set(row, column, getNextCell(row, column));
			}
		}
		currentGeneration = nextGeneration;
    }

}
