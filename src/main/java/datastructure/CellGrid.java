package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {
	private CellState[][] grid;

	public CellGrid(int rows, int columns, CellState initialState) {
		grid = new CellState[rows][columns];
		Arrays.stream(grid)
			.forEach(row -> {
				Arrays.fill(row, initialState);
			});
	}

	@Override
	public int numRows() {
		return grid.length;
	}

	@Override
	public int numColumns() {
		return grid[0].length;
	}

	@Override
	public void set(int row, int column, CellState element) {
		grid[row][column] = element;
	}

	@Override
	public CellState get(int row, int column) {
		return grid[row][column];
	}

	@Override
	public IGrid copy() {
		CellGrid newGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.ALIVE);
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[0].length; j++) {
				newGrid.set(i, j, get(i, j));
			}
		}
		return newGrid;
	}
    
}
